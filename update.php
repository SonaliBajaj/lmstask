<?php
include("connection.php");
$row = "";

$queryAuthor = mysqli_query($conn,"SELECT * FROM authors where a_id = '".$_GET['id']."'");

$resAuthor = mysqli_fetch_array($queryAuthor);

if(isset($_POST['submit']))
{
$row = '';
  $id = $_GET ['id'];
 

    $name = $_POST['flname'];
    $date = $_POST['date'];
   $gend  = $_POST['gender'];
    $mno = $_POST['mno'];
    $desc = $_POST['desc'];
 
    $q="update authors set name='".$name."',dob='".$date."',mobno='".$mno."',desci='".$desc."',gender='".$gend."' where a_id=$id";
    
    
     $ans=mysqli_query($conn,$q);
        
         if($ans)
         {
             echo 'Data Updated';
             header('location:display.php');
         }else{
             echo 'Data Not Updated';
         }
      }
     
    

?>

<?php
$nameErr = $genderErr = $mnoErr = $descErr ="";
$name = $desc = $gender = $mno ="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
      $nameErr = "Name is required";
    } else {
      $name = test_input($_POST["name"]);
      // check if name only contains letters and whitespace
      if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
        $nameErr = "Only letters and white space allowed";
      }
    }
    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
      } else {
        $gender = test_input($_POST["gender"]);
      }

      if (empty($_POST["desc"])) {
        $descErr = "Description is required";
      } else {
        $desc = test_input($_POST["desc"]);
      }
      if (empty($_POST["mno"])) {
        $mnoErr ="Contact Number is required";
      }
      
      else
      {
        $mno = test_input($_POST["mno"]);
        if(!preg_match('/^[6-9][0-9]{9}$/',$mno))
        {
            $mnoErr = "Only Integer Latter";
        }
      }
}
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
 
    <title>FORM</title>
</head>

<body style="background: linear-gradient( #ff9999, #ffcc99 ); background-repeat: no-repeat;">
    <div class="container">
    <form action="" method="POST"> 
     <h1>Authors Details :</h1>
        <table class="table" style="border:1px solid black;">
            <tr>
                <td>Name</td>
                <td><input type="text" class="form-control" name="flname" value="<?= $resAuthor['name']; ?>">  <?php echo $nameErr;?></span></td>
            </tr>
            <tr>
                <td>Date of Birth</td>
                <td><input type="date" name="date"  value="<?= $resAuthor['dob']; ?>" ></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td><input type="radio" name="gender" value="Male" <?php echo ($resAuthor['gender'] == 'm') ?  "checked" : "" ;  ?>>Male <br>
                <input type="radio" name="gender" value="Female" <?php echo ($resAuthor['gender'] == 'f') ?  "checked" : "" ;  ?>>Female
                <span class="error"> <?php echo $genderErr;?></span>
            </td>
            </tr>
            <tr>
                <td>Mobile No </td>
                <td><input type="text" name="mno" class="form-control"  value="<?= $resAuthor['mobno']; ?>"><?php echo $mnoErr;?></td>
            </tr>
            <tr>
                <td> Description</td>
                <td><textarea name="desc"  class="form-control"><?php echo $resAuthor["desci"];?></textarea><?php echo $descErr;?></span></td>
            </tr>
            <tr>
            <td></td>
            <td style="align:center"><input type="submit" value="submit" class="btn btn-warning btn-lg" name="submit"></td>
            </tr>
        </table>
    </form>
    </div>
</body>
</html>

